from django.contrib import admin
from django.urls import path, include
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework import permissions

from api.views import MainView

schema_view = get_schema_view(
    openapi.Info(
        title="Issue tracker API documentation",
        default_version='v1',
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
)
urlpatterns = [
    path('', MainView.as_view(), name='main'),
    path('admin/', admin.site.urls),
    path('api/', include(('api.urls', 'api'))),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='swagger'),
]
