from drf_yasg.inspectors import SwaggerAutoSchema


class CustomAutoSchema(SwaggerAutoSchema):

    def get_tags(self, operation_keys=None):
        """
        If the operation has a tag, use it. If the view has a tag, use it. If neither, use the operation key

        :param operation_keys: The keys of the operation
        :return: The tags for the operation.
        """
        return self.overrides.get('tags', None) or getattr(self.view, 'tags', []) or [operation_keys[0]]