
# Issue Tracker

## Project deployment
See [Remote deployment guide](./docs/deploy.md).

## Local deployment

### Python (via `venv`)

Make sure you have python's venv module available. Then initialize the new venv:
```
python3 -m venv venv
```

Then, declare the newly created environment:
```
source venv/bin/activate
```

Install requirements
```
pip install -r requirements.txt
```

Create `.env` file (you can base in on `.env.example`)

You may now export the local environment variables following way (specify path to the env-file):
```
export $(grep -v '^#' .env | xargs)
```

Regular Django manager commands are then available:
```
python manage.py collectstatic
python manage.py createsuperuser

python3 manage.py migrate
python3 manage.py runserver
```

Make sure that you have live database that is accessible locally before launching.