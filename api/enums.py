from django.db import models


class BugStatuses(models.TextChoices):
    RESOLVED = 1, 'Resolved'
    UNRESOLVED = 2, 'Unresolved'
