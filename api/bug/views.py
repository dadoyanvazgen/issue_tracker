from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics
from rest_framework.exceptions import NotFound
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from api.bug.models import Bug, Comment
from api.bug.serializers import BugSerializer, CommentSerializer
from api.filters import IsOwnerFilterBackend


class BugViewSet(ModelViewSet):
    """
    This class is a ModelViewSet that allows you to create, retrieve, update, and delete Bug objects
    """
    queryset = Bug.objects.all()
    permission_classes = (IsAuthenticated,)
    tags = ('Bug',)
    serializer_class = BugSerializer
    filterset_fields = ('status',)
    filter_backends = (IsOwnerFilterBackend, DjangoFilterBackend)

    def get_object(self):
        """
        It gets the object from the database, but only if the user is the owner of the object
        :return: The bug object with the primary key of pk and the owner of owner.
        """
        try:
            owner = self.request.user
            pk = self.kwargs.get('pk')
            return Bug.objects.get(pk=pk, owner=owner)

        except Bug.DoesNotExist as e:
            raise NotFound() from e


class AddCommentView(generics.CreateAPIView):
    """
    This class is a CreateAPIView that creates a new comment for a given bug
    """
    queryset = Comment.objects.all()
    permission_classes = (IsAuthenticated,)
    tags = ('Comment',)
    serializer_class = CommentSerializer


class DeleteCommentView(generics.DestroyAPIView):
    """
    This view deletes a comment.
    """
    queryset = Comment.objects.all()
    permission_classes = (IsAuthenticated,)
    tags = ('Comment',)

    def get_object(self):
        """
        It returns the bug object with the primary key of pk, and the owner of the bug is the user who is making the request
        :return: The bug object with the primary key of pk and the owner of the bug is the user.
        """

        try:
            owner = self.request.user
            pk = self.kwargs.get('pk')
            return Comment.objects.get(pk=pk, bug__owner=owner)

        except Comment.DoesNotExist as e:
            raise NotFound() from e
