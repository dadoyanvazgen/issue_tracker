from django.db import models

from api.enums import BugStatuses


class Bug(models.Model):
    owner = models.ForeignKey(
        to='api.User',
        on_delete=models.CASCADE,
        related_name='bugs',
        null=False,
        blank=True
    )
    title = models.CharField(
        max_length=100,
        null=False,
        blank=False
    )
    body = models.TextField(null=True, blank=True)
    status = models.CharField(
        choices=BugStatuses.choices,
        null=False,
        max_length=1,
        default=BugStatuses.UNRESOLVED
    )
    user = models.ForeignKey(
        to='api.User',
        on_delete=models.SET_NULL,
        related_name='assigned_bugs',
        null=True,
        blank=True
    )


class Comment(models.Model):
    bug = models.ForeignKey(
        to=Bug,
        on_delete=models.CASCADE,
        related_name='comments',
        null=False,
        blank=True
    )
    title = models.CharField(
        max_length=100,
        null=False,
        blank=False
    )
    body = models.TextField(null=True, blank=True)