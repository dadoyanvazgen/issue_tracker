from django.contrib.auth import get_user_model
from django.utils.translation import gettext as _

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from api.bug.models import Bug, Comment

UserModel = get_user_model()


class CommentSerializer(serializers.ModelSerializer):
    bug_id = serializers.IntegerField(write_only=True, required=True)

    class Meta:
        model = Comment
        fields = (
            'id',
            'title',
            'body',
            'bug_id'
        )

    def validate_bug_id(self, bug_id):
        owner = self.context['request'].user
        if not Bug.objects.filter(pk=bug_id, owner=owner).exists():
            raise ValidationError(_('Bug does not exists'))
        return bug_id


class BugSerializer(serializers.ModelSerializer):
    comments = CommentSerializer(many=True, read_only=True)

    class Meta:
        model = Bug
        fields = (
            'id',
            'title',
            'body',
            'status',
            'user',
            'comments'
        )

    def create(self, validated_data):
        owner = self.context['request'].user

        validated_data['owner'] = owner
        return super().create(validated_data)
