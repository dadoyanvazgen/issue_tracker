from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import TokenRefreshView, TokenObtainPairView

from api.bug.views import BugViewSet, AddCommentView, DeleteCommentView

app_name = 'api'

__all__ = ['urlpatterns']

urlpatterns = [
    path('token/access', TokenObtainPairView.as_view(), name='token_access'),
    path('token/refresh', TokenRefreshView.as_view(), name='token_refresh'),

    #comment routes
    path('comment', AddCommentView.as_view(), name='add_comment'),
    path('comment/<int:pk>', DeleteCommentView.as_view(), name='delete_comment'),

]


router = DefaultRouter(trailing_slash=False)

# bug CRUD
router.register('bug', BugViewSet, 'bug')

urlpatterns += router.urls
