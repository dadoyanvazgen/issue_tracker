from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from api.bug.models import Bug, Comment
from api.models import User


admin.site.register(User, UserAdmin)
admin.site.register(Bug)
admin.site.register(Comment)