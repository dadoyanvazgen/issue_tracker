from django.shortcuts import render
from django.views import View


# Create your views here.
class MainView(View):
    template = 'index.html'

    def get(self, request):
        """
        It takes a request, renders a template, and returns the rendered template

        :param request: The request object
        :return: The get method is returning the request, the template, and the context.
        """
        return render(request, self.template)
