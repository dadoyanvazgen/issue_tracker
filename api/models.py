from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models


class User(AbstractUser):
    created_at = models.DateTimeField(
        auto_now_add=True,
        null=True,
        blank=True
    )
    updated_at = models.DateTimeField(
        auto_now=True,
        null=True
    )
    USERNAME_FIELD = 'username'
    objects = UserManager()

    def __str__(self):
        return self.username or f'User object ID:{self.pk}'
